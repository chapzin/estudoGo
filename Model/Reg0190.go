package Model

import "time"

type Reg0190 struct {
	Reg   string
	Unid  string
	Descr string
	DtIni time.Time
	DtFin time.Time
	Cnpj  string
}
